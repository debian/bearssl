#include <assert.h>
#include <stdio.h>
#include "bearssl.h"

int main(void) {

    const char *name = 0;

    assert(br_prng_seeder_system(&name) != 0);
    assert(name != 0);
    assert(strlen(name) != 0);

    printf("%s\n", name);
    return 0;
}
