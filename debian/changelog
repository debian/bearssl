bearssl (0.6+dfsg.1-4) unstable; urgency=medium

  * d/gbp.conf add
  * d/control: bump Standards-Version to 4.7.0, no changes
  * d/salsa-ci.yml add and enable wrap-and-sort -asbkt
  * d/copyright: bump my copyright year
  * d/rules: move installation libraries to d/*.install files
  * d/rules: fix removing files in override_dh_auto_clean
  * d/t/02ecdh-x25519: add new ECDH x25519 test
  * d/t/02ecdh-nistp256: add new ECDH nistp256 test
  * d/{control,copyright}: update my email to "janmojzis@debian.org"
  * d/salsa-ci.yml: SALSA_CI_ENABLE_WRAP_AND_SORT=1
  * d/bearssl.h: add comment

 -- Jan Mojžíš <janmojzis@debian.org>  Sat, 25 Jan 2025 23:20:36 +0100

bearssl (0.6+dfsg.1-3) unstable; urgency=medium

  * d/control: add Multi-Arch: same
  * d/control: bump Standards-Version to 4.6.1, no changes
  * d/copyright: update my copyright year
  * d/libbearssl-dev.lintian-overrides remove

 -- Jan Mojžíš <jan.mojzis@gmail.com>  Fri, 08 Jul 2022 17:03:42 +0200

bearssl (0.6+dfsg.1-2) unstable; urgency=medium

  * Install also /usr/include/bearssl.h header file which only includes
    /usr/include/bearssl/bearssl.h. It allows one to compile code without
    '-I/usr/include/bearssl'.
  * d/rules: add DEB_CFLAGS_MAINT_APPEND = -Os

 -- Jan Mojžíš <jan.mojzis@gmail.com>  Mon, 22 Nov 2021 07:11:18 +0100

bearssl (0.6+dfsg.1-1) unstable; urgency=medium

  * Initial release. (Closes: #982135)
  * dfsg version: The upstream bearssl-0.6 package contains precompiled
    windows-binary T0Comp.exe. The binary is not needed to compile the Debian
    package and affects the source package. Therefore, dfsg is a repackaged
    version without the T0Comp.exe binary file.

 -- Jan Mojžíš <jan.mojzis@gmail.com>  Sun, 10 Oct 2021 09:47:27 +0200
